package space.schober.adapternachten;

import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
	static Logger logger = Logger.getAnonymousLogger();
	private static final Level LOG_LEVEL = Level.FINEST;

	public static void main(String[] args) {
		logger.setLevel(LOG_LEVEL);

		ConsoleHandler handler = new ConsoleHandler();
		handler.setLevel(LOG_LEVEL);
		logger.addHandler(handler);

		if (args.length == 0) {
			logger.log(Level.SEVERE, "Please pass the password as the first argument!");
			throw new IllegalArgumentException("Missing password!");
		}

		Set<Person> persons = ReaderWriter.readList("src/main/java/io/testing.csv");
		Person.addLinkedPersons(persons);
		Email email = new Email("adapternachten@gmail.com", args[0], "Secret Schober");

		for (Person person : persons) {
			HTML html = new HTML("src/main/java/io/email/secret-santa-2024");
			html.generateHTML(person);
			email.sendEmail(person, "Secret Schober", html);
		}
	}
}
