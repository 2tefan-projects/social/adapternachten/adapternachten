package space.schober.adapternachten;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Name {
	private static Logger logger = App.logger;

	private String firstName;
	private String lastName;
	private String title;

	public Name(String firstName, String lastName) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
	}

	public Name(String firstName, String lastName, String title) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setTitle(title);
	}

	@Override
	public String toString() {
		return String.format("first name: %s, last name: %s", this.getFirstName(), this.getLastName());
	}

	public String getFirstName() {
		return firstName;
	}

	public String getCompleteName() {
		if (this.getTitle() != null)
			return String.format("%s %s %s", this.getTitle(), this.getFirstName(), this.getLastName());
		else
			return String.format("%s %s", this.getFirstName(), this.getLastName());
	}

	public void setFirstName(String firstName) {
		if (isStringValid(firstName))
			this.firstName = firstName;
		else
			logger.log(Level.SEVERE, "Invalid first name!");
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (isStringValid(lastName))
			this.lastName = lastName;
		else
			logger.log(Level.SEVERE, "Invalid last name!");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		if (isStringValid(lastName))
			this.title = title;
		else
			logger.log(Level.SEVERE, "Invalid title!");
	}

	private boolean isStringValid(String s) {
		return !(s != null && s.equals(""));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Name other = (Name) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
}
