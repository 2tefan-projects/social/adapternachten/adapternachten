package space.schober.adapternachten;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.EmailValidator;

public class Person {
	private static Logger logger = App.logger;

	private Name name;
	private String email;
	private LocalDate birthday;
	private Person linkedPerson;
	private static Random rng = new Random();

	public Person(Name name, String email, LocalDate birthday) {
		this.setName(name);
		this.setEmail(email);
		this.setBirthday(birthday);
	}

	@Override
	public String toString() {
		StringBuilder stb = new StringBuilder();
		stb.append(String.format("name: %s, e-mail: %s, birthday: %s, linked person: [ ", this.getName(),
				this.getEmail(), this.getBirthdayString()));
		if (this.getLinkedPerson() != null)
			stb.append(this.getLinkedPerson().getName());
		else
			stb.append("none");
		stb.append(" ]");

		return stb.toString();
	}

	public static void addLinkedPersons(Set<Person> persons) {
		ArrayList<Person> personsList = new ArrayList<>();
		ArrayList<Person> personsNotChoosenYet = new ArrayList<>();
		personsList.addAll(persons);
		personsNotChoosenYet.addAll(persons);

		try {
			Person potentialReceiver;
			for (int i = 0; i < personsList.size(); i++) {
				Person person = personsList.get(i);

				do {
					potentialReceiver = personsNotChoosenYet.get(rng.nextInt(personsNotChoosenYet.size()));
					person.setLinkedPerson(potentialReceiver);
					logger.log(Level.FINEST, "Looping...");
					logger.log(Level.FINEST, String.format("%s -> %s", person, potentialReceiver));
				} while (!isValidPerson(person, potentialReceiver, personsNotChoosenYet, personsList, i));

				logger.log(Level.FINEST, "x");
				personsNotChoosenYet.remove(potentialReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isValidPerson(Person currentPerson, Person potentialReceiver,
			List<Person> personsNotChoosenYet, ArrayList<Person> personsList, int currentPosition) throws Exception {
		if (currentPerson.getLinkedPerson().equals(currentPerson))
			return false;

		if (personsNotChoosenYet.size() > 2 || personsNotChoosenYet.size() == 1)
			return true;

		ArrayList<Person> remainingPersons = new ArrayList<>();
		remainingPersons.addAll(personsNotChoosenYet);

		if (!remainingPersons.remove(potentialReceiver) || remainingPersons.size() != 1)
			throw new Exception("Person not in list!");

		Person lastPersonAvailableToBeChoose = remainingPersons.get(0);
		Person nextPerson = personsList.get(currentPosition + 1);

		return !lastPersonAvailableToBeChoose.equals(nextPerson);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((linkedPerson == null) ? 0 : linkedPerson.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (linkedPerson == null) {
			if (other.linkedPerson != null)
				return false;
		} else if (!linkedPerson.equals(other.linkedPerson))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public static boolean isValidEmailAddress(String email) {
		boolean allowLocal = true;
		EmailValidator ev = new EmailValidator(allowLocal, true, DomainValidator.getInstance(allowLocal));
		return ev.isValid(email);
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (isValidEmailAddress(email))
			this.email = email;
		else
			logger.log(Level.SEVERE, String.format("Invalid email! %s", email));
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public String getBirthdayString() {
		return birthday.toString();
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public Person getLinkedPerson() {
		return linkedPerson;
	}

	public void setLinkedPerson(Person linkedPerson) {
		this.linkedPerson = linkedPerson;
	}
}
