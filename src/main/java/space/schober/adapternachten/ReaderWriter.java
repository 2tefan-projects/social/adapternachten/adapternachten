package space.schober.adapternachten;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReaderWriter {
	private static Logger logger = App.logger;

	public static Set<Person> readList(String path) {
		return readList(path, ";");
	}

	public static Set<Person> readList(String path, String separator) {
		HashSet<Person> persons = new HashSet<>();

		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line = br.readLine();
			String[] tokens;

			while (br.ready()) {
				line = br.readLine();
				tokens = line.split(separator);

				persons.add(new Person(new Name(tokens[0], tokens[1]), tokens[2],
						LocalDate.parse(tokens[3], DateTimeFormatter.ISO_DATE)));
			}

		} catch (FileNotFoundException e) {
			logger.log(Level.SEVERE, "File not found!");
			e.printStackTrace();
		} catch (IOException e1) {
			logger.log(Level.SEVERE, "IOException!");
			e1.printStackTrace();
		}

		return persons;
	}
}
