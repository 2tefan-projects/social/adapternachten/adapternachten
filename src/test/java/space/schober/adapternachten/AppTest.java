package space.schober.adapternachten;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	public void testNoEndlessLoopWhenChoosingLinkedPeople() throws Exception {
		Set<Person> persons = new HashSet<>();
		Person p1 = new Person(new Name("Test", "Klein"), "test@localhost2.com", LocalDate.now());
		Person p2 = new Person(new Name("Wolfgang", "Groß"), "test@localhost3.com", LocalDate.now());
		Person p3 = new Person(new Name("Lea", "Mittel"), "test@localhost4.com", LocalDate.now());
		persons.add(p1);
		persons.add(p2);
		persons.add(p3);
		

		ArrayList<Person> personsList = new ArrayList<>();
		ArrayList<Person> personsNotChoosenYet = new ArrayList<>();
		personsList.addAll(persons);
		personsNotChoosenYet.addAll(persons);
		
		p1.setLinkedPerson(p2);
		personsNotChoosenYet.remove(p2);
		
		p2.setLinkedPerson(p1);
		assertEquals(false,Person.isValidPerson(p2, p1, personsNotChoosenYet, personsList, 1));
	}

	public void testComparision() {
		Person p1 = new Person(new Name("Test", "Klein"), "test@localhost.com", LocalDate.now());
		Person p2 = new Person(new Name("Wolfgang", "Groß"), "test@localhost.com", LocalDate.now());
		Person p3 = new Person(new Name("Lea", "Mittel"), "test@localhost.com", LocalDate.now());

		assertEquals(false, p1.equals(p2));
		assertEquals(false, p2.equals(p3));
		assertEquals(false, p1.equals(p3));
	}
}
